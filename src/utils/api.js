const axios = require('axios');
const client = localStorage.getItem('client') || null;
const accessToken = localStorage.getItem('access-token') || null;
const {uid} = JSON.parse(localStorage.getItem('user')) || {uid:null}
console.log('api:', process.env)
const api = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  headers: { "Cache-control": "no-cache", "client": client, "access-token": accessToken, "uid": uid }
});
axios.interceptors.request.use(
  config => {
    const client = localStorage.getItem('client') || null;
    const accessToken = localStorage.getItem('access-token') || null;
    const { uid } = JSON.parse(localStorage.getItem('user')) || { uid: null }
    if (accessToken || uid) {
      config.headers['accessToken'] = accessToken;
      config.headers['uid'] = uid;
      config.headers['client'] = client;
    }
    // config.headers['Content-Type'] = 'application/json';
    return config;
  },
  error => {
    Promise.reject(error)
  });
api.interceptors.response.use(function (response) {
  // response.headers
  if (response.headers['access-token'] && response.headers['client']) {
    console.log(response.headers);
    localStorage.setItem('access-token', response.headers['access-token'] )
    localStorage.setItem('client', response.headers['client'] )
  }
  console.log(response.headers);
  return response;
}, function (error) {
  return error
});

export default api;