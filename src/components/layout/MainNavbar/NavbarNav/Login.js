import React from "react";
import {
  Button,
  Modal,
  ModalBody,
  Col,
  Form,
  FormGroup,
  FormInput,
  Row,
  Alert
} from "shards-react";
import api from "../../../../utils/api";
import Loader from 'react-loader-spinner'

export default class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      visibleError: false,
      open: false,
      loader: false,
      username: '',
      password: '',
    };
    this.toggle = this.toggle.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
  }

  handleChange(event) {
    this.setState({ [event.target.id]: event.target.value });
  }
  
  toggle() {
    console.log(this.state.open)
    this.setState({
      open: !this.state.open
    });
  }
  handleLogin(form) {
    form.preventDefault();
    this.setState({
      loader: true,
      visibleError: false
    });
    api.post('auth/sign_in', {
      email: this.state.username,
      password: this.state.password
    }).then(response => {
      if (response.status !== 200) {
        console.log(response.response.data.errors)
        // this.setState({ visibleError: 'Fill the form correctly'});
        this.setState({ visibleError: response.response.data.errors[0] });
      } else {
        localStorage.setItem('user', JSON.stringify(response.data.data))
        window.location.reload();
      }
    }).finally(() => {
      this.setState({
        loader: false
      })
    });
  }

  render() {
    return (
      <>
        <Button
          style={{ margin: 10, marginLeft: 0 }}
          onClick={this.toggle}
        >Login</Button>
        <Modal open={this.state.open} toggle={this.toggle}>
          <ModalBody>
            <Col lg="12" md="12" sm="12" style={{textAlign:'center'}}>
              <img
                src={require('../../../../assets/logo.png')}
                style={{ width: 120, marginLeft: 10 }}
              />
            </Col>
            <Col lg="12" md="12" sm="12" style={{marginTop:20}}>
              <Alert open={this.state.visibleError} theme="danger">
                {this.state.visibleError} &rarr;
              </Alert>
              <Form onSubmit={this.handleLogin}>
                <FormGroup>
                  <label htmlFor="#username">User</label>
                  <FormInput id="username" value={this.state.username} onChange={this.handleChange}/>
                </FormGroup>
                <FormGroup>
                  <label htmlFor="#password">Password</label>
                  <FormInput type="password" id="password" value={this.state.password} onChange={this.handleChange}/>
                </FormGroup>
                <Row>
                  <Col lg="9" md="9" md="9">
                    <Button outline theme="secondary">Cancel</Button>
                  </Col>
                  <Col lg="3" md="3" md="3">
                    <Button block type="submit">
                      {
                        this.state.loader ?
                          <Loader
                            type="TailSpin"
                            color="#fff"
                            height={15}
                            width={15}
                          />
                        :
                        'Login'
                      }
                    </Button>
                  </Col>
                </Row>
              </Form>
            </Col>
          </ModalBody>
        </Modal>
      </>
    );
  }
}
