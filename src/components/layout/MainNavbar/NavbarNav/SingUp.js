import React from "react";
import {
  Button,
  Modal,
  ModalBody,
  Col,
  Form,
  FormGroup,
  FormInput,
  Row,
  Alert,
  FormFeedback
} from "shards-react";
import PageTitle from "../../../../components/common/PageTitle";
import api from "../../../../utils/api";
import Loader from "react-loader-spinner";
import SweetAlert from 'react-bootstrap-sweetalert';

export default class SingUp extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      visible: false,
      open: false,
      loader: false,
      visibleError: false,
      showSuccess: false,
      invalidPassword: false,
      invalidEmail: false,
      email: '',
      name: '',
      nickname: '',
      password: '',
    };
    this.toggle = this.toggle.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSingUp = this.handleSingUp.bind(this);
  }

  handleChange(event) {
    this.setState({ [event.target.id]: event.target.value });
  }

  handleSingUp(form) {
    form.preventDefault();
    if (!this.checkForm()) return false;

    this.setState({
      loader: true,
      visibleError: false
    });
    api.post('auth', {
      email: this.state.email,
      name: this.state.name,
      nickname: this.state.nickname,
      password: this.state.password,
      password_confirmation: this.state.password
    }).then(response => {
      console.log(response)
      if (response.status !== 200) {
        console.log(response.response.data.errors.full_messages)
        // this.setState({ visibleError: 'Fill the form correctly'});
        this.setState({ visibleError: response.response.data.errors.full_messages});
      }else{
        this.toggle();
        this.setState({ showSuccess: true });
      }
      // localStorage.setItem('user', JSON.stringify(response.data.data))
    }).catch(error => {
      console.log(error.response);
    }).finally(() => {
      this.setState({
        loader: false
      })
    });
  }

  checkForm() {
    var parse_email = /^[a-z0-9.]+@[a-z0-9]+\.[a-z]+(\.[a-z]+)?$/i;
    let chekEmail = parse_email.test(this.state.email);
    !chekEmail ? this.setState({ invalidEmail: true }) : this.setState({ invalidEmail: false });
    let checkPassword = this.state.password.length >= 8;
    console.log(this.state.password.length >= 8, this.state.password.length);
    !checkPassword ? this.setState({ invalidPassword: true }) : this.setState({ invalidPassword: false });
    return !(!chekEmail || !checkPassword);
  }

  toggle() {
    console.log(this.state.open)
    this.setState({
      open: !this.state.open
    });
  }

  render() {
    return (
      <>
        <Button
          style={{ margin: 10 }}
          onClick={this.toggle}
        >SingUp</Button>
        <Modal open={this.state.open} toggle={this.toggle}>
          <ModalBody>
            <Col lg="12" md="12" sm="12" style={{textAlign:'center', paddingLeft: 0}}>
              <img src={require('../../../../images/icon/close.svg')} height="15" className="close" onClick={this.toggle}/>
              <PageTitle sm="11" title="Create your account" subtitle="Join Fast Event" className="text-sm-left" />
            </Col>
            <Col lg="12" md="12" sm="12" style={{marginTop:20}}>
              {this.state.visibleError && this.state.visibleError.map(item => (
                <Alert open={item} theme="danger">
                  {item} &rarr;
                </Alert>
              ))}
              <Form onSubmit={this.handleSingUp}>
                <FormGroup>
                  <label htmlFor="#email">Email</label>
                  <FormInput required invalid={this.state.invalidEmail} id="email" type="email" value={this.state.email} onChange={this.handleChange}/>
                  <FormFeedback>Enter a valid emailaracters.</FormFeedback>
                </FormGroup>
                <FormGroup>
                  <label htmlFor="#name">Name</label>
                  <FormInput required id="name" value={this.state.name} onChange={this.handleChange}/>
                </FormGroup>
                <FormGroup>
                  <label htmlFor="#nickname">Nickname</label>
                  <FormInput required id="nickname" value={this.state.nickname} onChange={this.handleChange}/>
                </FormGroup>
                <FormGroup>
                  <label htmlFor="#password">Password</label>
                  <FormInput required invalid={this.state.invalidPassword} type="password" id="password" value={this.state.password} onChange={this.handleChange}/>
                  <FormFeedback>The password must contain at least 8 characters.</FormFeedback>
                </FormGroup>
                <Row className="m-t-30">
                  <Col lg="12" md="12" md="12">
                    <Button block type="submit">
                      {
                        this.state.loader ?
                          <Loader
                            type="TailSpin"
                            color="#fff"
                            height={15}
                            width={15}
                          />
                          :
                          'Create'
                      }
                    </Button>
                  </Col>
                </Row>
              </Form>
            </Col>
          </ModalBody>
        </Modal>
        <SweetAlert 
          success 
          title="Good job!" 
          onConfirm={() => this.setState({showSuccess:false})}
          show={this.state.showSuccess}
          timeout={2000}>
          Successful registration.
        </SweetAlert>
      </>
    );
  }
}
