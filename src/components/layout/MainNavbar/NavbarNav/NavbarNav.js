import React from "react";
import { Nav, Row } from "shards-react";

import AddEvent from "./AddEvent";
import UserActions from "./UserActions";
import Login from './Login';
import SingUp from './SingUp';

export default () => {
  const userdata = JSON.parse(localStorage.getItem('user')) || null;
  return (
  userdata ?
  <Nav navbar className="flex-row" expand="lg">
    <AddEvent />
    <UserActions />
  </Nav>
  :
  <Row style={{marginRight:15}}>
    <SingUp />
    <Login />
  </Row>
  )
};
