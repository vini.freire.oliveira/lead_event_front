import React from "react";
import {
  Button,
  Modal,
  ModalBody,
  ModalHeader,
  ModalFooter,
  Form,
  FormGroup,
  FormInput
} from "shards-react";
import {
  Link
} from "react-router-dom";
import api from "../../../../utils/api";

export default class Notifications extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      visible: false,
      open: false
    };
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    console.log(this.state.open)
    this.setState({
      open: !this.state.open
    });
  }

  handleSubmit(form) {
    form.preventDefault();
    console.log(form)
  }

  render() {
    return (
      <>
        <Link to="/add-new-event">
          <Button
            outline
            style={{ marginTop: 10, marginBottom: 10 }}
            onClick={this.toggle}
          >+ Register event</Button>
        </Link>
        {/* <Modal open={this.state.open} toggle={this.toggle}>
          <ModalHeader>Register Event</ModalHeader>
          <ModalBody>
            <Form onSubmit={this.handleSubmit}>
              <FormGroup>
                <label htmlFor="#username">Name</label>
                <FormInput id="#username"/>
              </FormGroup>
              <FormGroup>
                <label htmlFor="#password">Time</label>
                <FormInput type="password" id="#password" />
              </FormGroup>
              <FormGroup>
                <label htmlFor="#password">Location</label>
                <FormInput type="password" id="#password"/>
              </FormGroup>
              <Button theme="success" type="submit">
                Save
            </Button>
            </Form>
          </ModalBody>
          <ModalFooter>
            <Button outline theme="danger" onClick={this.toggle}>
              Cancel
            </Button>
            <Button theme="success">
              Save
            </Button>
          </ModalFooter>
        </Modal> */}
      </>
    );
  }
}
