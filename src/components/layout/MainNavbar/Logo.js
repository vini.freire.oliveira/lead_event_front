import React from "react";
import {
  NavbarBrand
} from "shards-react";

export default () => (
  <NavbarBrand href="#" className="w-60 d-none d-md-flex d-lg-flex">
    <img
      src={require('../../../assets/logo.png')}
      style={{width:96, marginLeft: 10}}
    />
  </NavbarBrand>
);
