import React from "react";
import PropTypes from "prop-types";

import ReactQuill from "react-quill";
import { Card, CardBody, Form, FormInput, Row, Col, DatePicker } from "shards-react";

import "react-quill/dist/quill.snow.css";
import "../../assets/quill.css";

const Event = ({data}) => {
  const handleChange = (event) => {
    data[event.target.id] = event.target.value
    console.log(data)
  }
  return (
  <Card small className="mb-3">
    <CardBody>
      <Form className="add-new-post">
        <FormInput size="lg" className="mb-3" placeholder="Name" id="name" onChange={handleChange}/>
        <Row form>
          <Col md="3">
            <DatePicker size="lg" className="mb-3" placeholder="Date" style={{width:'100%'}}/>
          </Col>
          <Col md="3" className="form-group">
            <DatePicker 
              size="lg" 
              className="mb-3" 
              placeholder="Date" 
              style={{ width: '100%' }} 
              showTimeSelect
              showTimeSelectOnly
              timeIntervals={15}
              timeCaption="Time"
              dateFormat="h:mm aa"
            />
          </Col>
        </Row>
        <ReactQuill className="add-new-post__editor mb-1" />
      </Form>
    </CardBody>
  </Card>
  );
}

Event.propTypes = {
  data: PropTypes.object
};
export default Event;
