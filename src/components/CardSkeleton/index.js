import React from "react";
import { Row, Col, Card, CardBody } from "shards-react";
import Skeleton from 'react-loading-skeleton';
import PropTypes from "prop-types";


const CardSkeleton = ({ items, ...attrs }) => {
  items = items ? items : 1;
  const itemsSkeleton = []

  for (let i = 0; i < items; i++) {
    itemsSkeleton.push(
      <Col lg="3" md="6" sm="12" className="mb-4" key={i}>
        <Card small className="card-post card-post--1">
          <div
            className="card-post__image"
          >
            <Skeleton height={150} />
          </div>
          <CardBody>
            <Skeleton count={3} />
          </CardBody>
        </Card>
      </Col>
    )
  }

  return (
    itemsSkeleton
  )
};

CardSkeleton.propTypes = {
  /**
   * The number of repeat.
   */
  items: PropTypes.number,
};

export default CardSkeleton;
