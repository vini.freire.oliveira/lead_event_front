import React from "react";
import { 
  Container, 
  Card, 
  CardBody, 
  Form, 
  FormInput, 
  Row, 
  Col, 
  DatePicker, 
  FormGroup, 
  CardHeader,
  ListGroup,
  ListGroupItem,
  Button,
  FormCheckbox,
  Alert, 
} from "shards-react";
import moment from 'moment';
import TagsInput from 'react-tagsinput'
import 'react-tagsinput/react-tagsinput.css'
import PageTitle from "../components/common/PageTitle";
import Event from "../components/add-new-event/Event";
import SidebarActions from "../components/add-new-event/SidebarActions";
import SidebarCategories from "../components/add-new-event/SidebarCategories";
import { Link } from "react-router-dom";
import Skeleton from "react-loading-skeleton";
import api from "../utils/api";
import Loader from "react-loader-spinner";
import SweetAlert from "react-bootstrap-sweetalert";
const KeyCodes = {
  comma: 188,
  enter: 13,
};
const delimiters = [KeyCodes.comma, KeyCodes.enter];

class AddNewEvent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      menuVisible: false,
      loadArtist: false,
      loader: false,
      showSuccess: false,
      visibleError: false,
      event: {name:'', date:'', hour: ''},
      generes: [],
      artists: []
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeGeneres = this.handleChangeGeneres.bind(this);
    this.handleChangeArtists = this.handleChangeArtists.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.getArtists();
  }

  handleChange(input) {
    let event = { ...this.state.event, [input.target.id]: input.target.value}
    this.setState({ event: event });
  }

  handleChangeCustom(element, value) {
    let event = { ...this.state.event, [element]: value }
    this.setState({ event: event });
  }

  handleChangeGeneres(generes) {
    this.setState({ generes })
  }

  handleChangeArtists(artist, index) {
    artist.checked = !this.state.artists[index].checked
    this.setState({ ...this.state.artists, artist });
  }

  handleSubmit() {
    const {event, artists, generes} = this.state;
    let artists_ids = [];
    artists.forEach(item => {
      if (item.checked) {
        artists_ids.push(item.id)
      }
    });
    let data = {
      name: event.name,
      time: `${moment(event.date).format('MM-DD-YYYY')} ${moment(event.hour).format('HH:mm')}`,
      "tz": Intl.DateTimeFormat().resolvedOptions().timeZone,
      "location": event.location,
      "artists_ids": artists_ids,
      "genres": generes
    }
    console.log(data);
    this.setState({
      loader: true,
      visibleError: false
    });
    api.post('api/v1/events', data).then(response => {
      console.log(response)
      if (response.status !== 200) {
        console.log(response.response)
        // this.setState({ visibleError: 'Fill the form correctly'});
        this.setState({ visibleError: response.response.data });
      } else {
        this.setState({ showSuccess: true });
        this.props.history.push('/'); 
      }
      // localStorage.setItem('user', JSON.stringify(response.data.data))
    }).catch(error => {
      console.log(error.response);
    }).finally(() => {
      this.setState({
        loader: false
      })
    });

  }

  async getArtists() {
    this.setState({ loadArtist: true })
    await api.get('api/v1/artists').then(response => {
      console.log(response.data)
      if (response.data) {
        this.setState({ artists: response.data.map(item => ({...item, checked: false})) })
      }
    });
    this.setState({ loadArtist: false })
  }

  render() {
    return (
      <Container fluid className="main-content-container px-4 pb-4">
        {/* Page Header */}
        <Row noGutters className="page-header py-4">
          <Col sm="1" md="1" lg="1">
            <Link to="/">
              <div style={{ background: '#e4e4e4', borderRadius: 100, height: 50, width: 50, display: 'flex', justifyContent: 'center' }}>
                <img
                  src={require('../images/icon/back.svg')}
                  style={{ width: 25 }}
                />
              </div>
            </Link>
          </Col>
          <PageTitle sm="4" title="Add New Event" subtitle="Fast Event" className="text-sm-left" />
        </Row>

        <Row>
          {/* Event */}
          {
            this.state.visibleError && Object.keys(this.state.visibleError).map(key => (
              <Col lg="12" md="12">
                <Alert open={true} theme="danger">
                  {key}: {this.state.visibleError[key]} &rarr;
                </Alert>
              </Col>
            ))
          }
          <Col lg="7" md="12">
            <Card small className="mb-3">
              <CardBody>
                <Form className="add-new-post">
                  <FormGroup>
                    <label htmlFor="#username">Name</label>
                    <FormInput size="lg" required className="mb-3" id="name" onChange={this.handleChange} />
                  </FormGroup>
                  <FormGroup>
                    <label htmlFor="#username">Location</label>
                    <FormInput size="lg" required className="mb-3" id="location" onChange={this.handleChange} />
                  </FormGroup>
                  <Row form>
                    <Col md="3">
                      <FormGroup>
                        <label htmlFor="#username">Date</label>
                        <DatePicker size="lg" selected={this.state.event.date} className="mb-3" id="date" style={{ width: '100%' }} onChange={(data) => this.handleChangeCustom('date', data)}/>
                      </FormGroup>
                    </Col>
                    <Col md="3" className="form-group">
                      <FormGroup>
                        <label htmlFor="#username">Hour</label>
                        <DatePicker
                          size="lg"
                          className="mb-3"
                          placeholder="Date"
                          style={{ width: '100%' }}
                          selected={this.state.event.hour}
                          onChange={(data) => this.handleChangeCustom('hour', data)}
                          showTimeSelect
                          showTimeSelectOnly
                          timeIntervals={15}
                          timeCaption="Time"
                          dateFormat="h:mm aa"
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                  <FormGroup>
                    <label htmlFor="#username">Generes</label>
                    <TagsInput value={this.state.generes} required onChange={this.handleChangeGeneres} />
                  </FormGroup>
                </Form>
              </CardBody>
            </Card>
          </Col>

          {/* Sidebar Widgets */}
          <Col lg="5" md="12">
            <Card small className="mb-3">
              <CardHeader className="border-bottom">
                <h6 className="m-0">Artist</h6>
              </CardHeader>

              <CardBody className="p-0">
                <ListGroup flush>
                  <ListGroupItem className="px-3 pb-2">
                    {
                      this.state.loadArtist ? 
                        <Skeleton count={5} height={20} />
                      :
                        <div>
                          {
                            this.state.artists && this.state.artists.map((item, index) => (
                              <FormCheckbox
                                className="mb-1"
                                checked={item.checked}
                                onChange={e => this.handleChangeArtists(item, index)}
                              >
                                {item.name}
                              </FormCheckbox>
                            ))
                          }
                        </div>
                    }
                  </ListGroupItem>
                </ListGroup>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Button block onClick={this.handleSubmit}>
          {
            this.state.loader ?
              <Loader
                type="TailSpin"
                color="#fff"
                height={15}
                width={15}
              />
              :
              'Save'
          }
        </Button>
        <SweetAlert
          success
          title="Good job!"
          onConfirm={() => { 
            this.setState({ showSuccess: false });
          }}
          show={this.state.showSuccess}
          timeout={2000}>
          Successful registration.
        </SweetAlert>
      </Container>
    );
  }
}

export default AddNewEvent;
