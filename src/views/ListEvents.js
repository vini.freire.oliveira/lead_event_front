/* eslint jsx-a11y/anchor-is-valid: 0 */

import React from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  CardFooter,
  Badge,
  Button,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  FormInput
} from "shards-react";
import moment from 'moment';
import CardSkeleton from '../components/CardSkeleton';
import PageTitle from "../components/common/PageTitle";
import api from "../utils/api";

class ListEvents extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      Events: [],
      page: 1,
      search: ``
    };
    this.getEvents();
    this.getEvents = this.getEvents.bind(this);
    this.alterPage = this.alterPage.bind(this);

  }

  getEvents() {
    let search = this.state.search ? `&key=${this.state.search}` : '';
    api.get(`/api/v1/events?page=${this.state.page}${search}`).then(response => {
      console.log(response)
      this.setState({Events: response.data});
    });
  }

  alterPage(page) {
    this.setState({page: page, Events: []});
    setTimeout(() => this.getEvents(), 500);
  }

  render() {
    return (
      <Container fluid className="main-content-container px-4">
        {/* Page Header */}
        <Row noGutters className="page-header py-4">
          <PageTitle sm="4" title="Meet our events" subtitle="Events" className="text-sm-left" />
        </Row>
        <Row>
          <Col>
            <InputGroup seamless className="mb-3">
              <InputGroupAddon type="prepend">
                <InputGroupText>
                  <i className="material-icons">search</i>
                </InputGroupText>
              </InputGroupAddon>
              <FormInput onChange={(value) => { this.setState({ search: value.target.value }) }} />
              <InputGroupAddon type="append">
                <Button onClick={this.getEvents}>Search</Button>
              </InputGroupAddon>
            </InputGroup>
          </Col>
        </Row>
        {/* First Row of Posts */}
        <Row>
          {
            this.state.Events.length == 0 ?
              <CardSkeleton items={4} />
            :
            this.state.Events && this.state.Events.map((event, id) => {
            let bg = require(`../images/content-management/${id.toString()[id.toString().length - 1]}.jpeg`)
            return (
            <Col lg="3" md="6" sm="12" className="mb-4" key={id}>
              <Card small className="card-post card-post--1">
                <div
                  className="card-post__image"
                    style={{ backgroundImage: `url(${bg})` }}
                >
                  {
                    event.genres.map(genere => (
                      <Badge
                        pill
                        style={{margin:5}}
                        theme="info"
                      >
                        {genere}
                      </Badge>
                    ))
                  }
                  <div className="card-post__author d-flex">
                    {
                      event.artists.map(artist => (
                        <Badge
                          style={{ margin: 5 }}
                          theme="light"
                        >
                          {artist.name}
                        </Badge>
                      ))
                    }
                  </div>
                </div>
                <CardBody>
                  <h5 className="card-title">
                    <a href="#" className="text-fiord-blue">
                      {event.name}
                    </a>
                  </h5>
                  <p className="card-text d-inline-block mb-3">{event.location}</p>
                  <p className="text-muted">{moment(event.date).format('MM-DD-YYYY HH:mm')}</p>
                </CardBody>
              </Card>
            </Col>)
          })}
        </Row>
        <Row style={{marginBottom: 10}}>
          <Col>
          {
              this.state.page > 1 && <Button outline onClick={() => { this.alterPage(this.state.page - 1) }}>Previous</Button>
          }
          </Col>
          <Col style={{ textAlign: 'right' }}><Button outline onClick={() => { this.alterPage(this.state.page + 1 ) }}>Next</Button></Col>
        </Row>
      </Container>
    );
  }
}

export default ListEvents;
