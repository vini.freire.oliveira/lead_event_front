import React from "react";
import { Redirect } from "react-router-dom";

// Layout Types
import { DefaultLayout } from "./layouts";

// Route Views
import AddNewEvent from "./views/AddNewEvent";
import ListEvents from "./views/ListEvents";

export default [
  {
    path: "/",
    exact: true,
    layout: DefaultLayout,
    component: () => <Redirect to="/events" />
  },
  {
    path: "/add-new-event",
    layout: DefaultLayout,
    component: AddNewEvent
  },
  {
    path: "/events",
    layout: DefaultLayout,
    component: ListEvents
  }
];
